# The Producer

## Harvard Magazine Web Production CLI

Run `less README.md to read this in the terminal`

## How to Install

- Clone the repo.
- Run `npm install` (skip this step if there is a `node_modules` folder already).
- If installed already, always `git pull` before running the program to get the latest version.
- To start, run `node index.js`.
- If you get any sort of `module not found` error, run `npm install` again.

## Progress

- Articles: InCopy to CSV - ✅
- Articles: InCopy to HTML - ❌
- Class Notes - ✅
- Obituaries - ✅
- Classifieds - ⚠️ (Can't handle invalid CSV)

---

## How to Use

### Articles: InCopy to CSV

- Move the InCopy articles into the `articles` folder.
- Select `Articles: InCopy to CSV` from the list.
- Run `node index.js`.
- Confirm that you put the files in the right place.
- Select the issue month.
- Enter the node ID for the issue.
- If everything worked, you should see the number of rows created and a new CSV file created in `output` Any old file in `output` will be overridden unless you change the name of the old file.

---

- Open a new Excel window.
- Select `File > Import`.
- Choose `CSV file`.
- The `.csv` file you just made is in `output`.
- Select `Delimited` and change the file origin to `Unicode (UTF-8)`.
- Select `Comma` as the only delimiter.
- Finish.

### Articles: InCopy to HTML

In progress

### Class Notes

- Copy and paste the entire contents of `Notes.icml` into a new Word document.
- Save the Word document as `notes.docx` exactly.
- Move `notes.docx` into the `class-notes` folder.
- Run `node index.js`.
- Select `Class Notes` from the list.
- Confirm that you put the file in the right place.
- Select the issue month.
- Enter the node ID for the issue.
- If everything worked, you should see the number of rows created and a new CSV file created in `output` Any old file in `output` will be overridden unless you change the name of the old file.
- **Note:** The weird looking characters are HTML entities that will show up properly as long as you paste into `Disable rich-text` in the Drupal editor.
- Save and finish.

### Obituaries

- Copy and paste the entire contents of `Obits.icml` into a new Word document.
- Save the Word doc as `obits.docx` exactly.
- Move `obits.docx` into the `obituaries` folder.
- Run `node index.js`.
- Select `Obituaries` from the list.
- Confirm that you put the file in the right place.
- Select the issue month.
- Enter the node ID for the issue.
- If everything worked, you should see the number of rows created and a new CSV file created in `output`. Any old file in `output` will be overridden unless you change the name of the old file.
- Add the section names for `Graduate Schools` and `Faculty and Staff`.
- Remove the lines for `Graduate Schools` and `Faculty and Staff`.
- **Note:** The weird looking characters are HTML entities that will show up properly as long as you paste into `Disable rich-text` in Drupal.
- Save and Finish.

### Classifieds

Adds the necessary headers, replaces the Ad ID code with its value, and wraps the headline in `<strong>` tags.

- Move the classifieds spreadsheet into the `classifieds` folder.
- Rename the classifieds spreadsheet to `classifieds.csv` exactly.
- Run `node index.js`.
- Select `Classifieds` from the list.
- Confirm that you put the file in the right place.
- Select the issue month.
- Enter the node ID for the issue.
- If everything worked, you should see the number of rows created and a new CSV file created in `output`. Any old file in `output` will be overridden unless you change the name of the old file.
- Open the new classifieds file and delete every instance of `PLACEHOLDER"`.
- Save and finish.

### All

This just runs everything in order with the same prompts.
