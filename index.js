const inquirer = require('inquirer')
const classNotes = require('./modules/class-notes')
const classifieds = require('./modules/classifieds')
const incopyToCSV = require('./modules/incopy-to-csv')
const incopyToHTML = require('./modules/incopy-to-html')
const obituaries = require('./modules/obituaries')
const questions = [
  {
    name: 'action',
    message: 'What do you want to produce?',
    type: 'list',
    choices: [
      'Articles: InCopy to CSV', 'Articles: InCopy to HTML', 'Class Notes', 'Obituaries', 'Classifieds',
      new inquirer.Separator(),
      'All'
    ]
  },
  {
    name: 'confirm',
    message: 'Are the input files are in the correct folder?',
    type: 'confirm'
  },
  {
    name: 'issue',
    message: 'Which issue are you producing?',
    type: 'list',
    choices: [
      'January_February',
      'March_April',
      'May_June',
      'July_August',
      'September_October',
      'November_December'
    ],
    when: function (answers) {
      return answers.action !== 'Articles: InCopy to HTML'
    }
  },
  {
    name: 'nodeID',
    message: 'Node ID for this issue:',
    type: 'input',
    when: function (answers) {
      return answers.action !== 'Articles: InCopy to HTML'
    }
  }
]
inquirer.prompt(questions).then(answers => {
  if (answers.confirm) {
    switch (answers.action) {
      case 'Articles: InCopy to CSV':
        incopyToCSV.formatCSV(answers.nodeID, answers.issue)
        break
      case 'Articles: InCopy to HTML':
        incopyToHTML.formatHTML()
        break
      case 'Class Notes':
        classNotes.formatClassNotes(answers.nodeID, answers.issue)
        break
      case 'Obituaries':
        obituaries.formatObituaries(answers.nodeID, answers.issue)
        break
      case 'Classifieds':
        classifieds.formatClassifieds(answers.nodeID, answers.issue)
        break
      case 'All':
        incopyToHTML.formatHTML()
        incopyToCSV.formatCSV()
        classNotes.formatClassNotes()
        obituaries.formatObituaries()
        classifieds.formatClassifieds()
    }
  }
})
