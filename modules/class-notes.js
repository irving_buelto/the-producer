module.exports = {
  formatClassNotes: function (nodeID, issue) {
    const fs = require('fs')
    const path = require('path')
    const json2csv = require('json2csv')
    const mammoth = require('mammoth')
    const cheerio = require('cheerio')

    const fields = [
      'Note Title',
      'Issue (NID)',
      'School',
      'Class Year',
      'Body',
      'Published (0 for unpublished)'
    ]
    const data = []

    mammoth.convertToHtml({ path: path.join(__dirname, '../static/class-notes/notes.docx') }).then((result) => {
      const regex = /(<strong>|<b>)\d{4}(s|-\d{4})?/g
      const $ = cheerio.load(result.value)
      let classYear
      let issueAbbr = issue.split('_').map((el) => el.slice(0, 1)).join('')
      $('p').each(function (index) {
        const body = $(this).html()
        if (body.match(regex)) {
          classYear = body.match(regex)[0].slice(-4)
        } else if (body.match(/REUNION/g) || body.match(/(Harvard\s)?(<p>)?(Secretar(y|ies)?:)/gi) || body.match(/Please submit news/g) || body.match(/<strong>Arts &amp; Sciences/g)) {
          return true
        } else {
          data.push({
            'Note Title': `${issueAbbr} note #${data.length + 1}`,
            'Issue (NID)': nodeID,
            'School': classYear === '2017' ? 'Graduate School of Arts and Sciences' : 'Harvard College',
            'Class Year': classYear,
            'Body': `<p>${body}</p>`,
            'Published (0 for unpublished)': '1'
          })
        }
      })
      const csv = json2csv({ data: data, fields: fields })
      fs.writeFile(path.join(__dirname, `../static/output/class-notes_importer_${issue.toLowerCase()}_${(new Date()).getFullYear()}_template.csv`), csv, function (err) {
        if (err) throw err
        if (data.length === 0) {
          console.log('0 rows created. Double check the class-notes folder.')
        } else {
          console.log(`Success! ${data.length} rows created.`)
        }
      })
    }).done()
  }
}
