module.exports = {
  formatHTML: function () {
    const fs = require('fs')
    const path = require('path')
    const convert = require('xml-js')
    const traverse = require('traverse')
    const tag = require('html-tag')

    fs.readdirSync(path.join(__dirname, '../static/articles/'), function (err, files) {
      if (err) throw err
      return files
    }).filter((e) => {
      return e !== '.keep' && e !== '.DS_Store'
    }).forEach((sourceFile) => {
      const incopy = fs.readFileSync(path.join(__dirname, '../static/articles/', sourceFile), 'utf8')
      const convertedJSON = convert.xml2js(incopy, {compact: true})

      traverse(convertedJSON).reduce(function (acc, x) {
        if (this.key === 'CharacterStyleRange') {
          acc.push(x)
        }
        return acc
      }, []).forEach((ParagraphStyleRange) => {
        console.log('<p>')
        ParagraphStyleRange.forEach((CharacterStyleRange) => {
          if (CharacterStyleRange.Content) {
            console.log(CharacterStyleRange.Content)
            if (CharacterStyleRange.Content.length > 0) {
              CharacterStyleRange.Content.forEach((Content) => {
                console.log(Content._text)
              })
            } else {
              if (CharacterStyleRange.Content._text instanceof Array) {
                console.log(tag('span', CharacterStyleRange.Content._text[0]))
                console.log(CharacterStyleRange.Content._text[1])
              } else {
                if (CharacterStyleRange._attributes.FontStyle === 'Bold') {
                  console.log(tag('strong', CharacterStyleRange.Content._text))
                } else {
                  console.log(CharacterStyleRange.Content._text)
                }
              }
            }
          }
          if (CharacterStyleRange.Br) {
            console.log('</p>')
            console.log('<br />')
          }
        })
      })
    })
  }
}
