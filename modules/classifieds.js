module.exports = {
  formatClassifieds: function (nodeID, issue) {
    const fs = require('fs')
    const path = require('path')
    const csv = require('fast-csv')

    csv
      .fromPath(path.join(__dirname, '../static/classifieds/classifieds.csv'), { headers: ['Ad Category', 'Ad Type', 'Ad ID', 'Headline', 'Ad Content'], quote: '"', escape: '"', ignoreEmpty: true })
      .transform(function (obj) {
        switch (obj['Ad Type']) {
          case '107':
            obj['Ad Type'] = 'Display Ad'
            break
          case '100':
            obj['Ad Type'] = 'Text Ad'
            break
          case '103':
            obj['Ad Type'] = 'SEO Ad'
            break
        }

        return {
          'Ad Category': obj['Ad Category'],
          'Ad Type': obj['Ad Type'],
          'Ad ID': obj['Ad ID'].toString(),
          'Headline': obj['Headline'],
          'Ad Content': `<p><strong>${obj['Headline']}</strong>${obj['Ad Content']}</p>`,
          'Issue (NID)': nodeID,
          'Published(0 for unpublished)': '1'
        }
      })
      .pipe(csv.createWriteStream({ headers: ['Ad Category', 'Ad Type', 'Ad ID', 'Headline', 'Ad Content', 'Issue NID', 'Published(0 for unpublished)'] }))
      .pipe(fs.createWriteStream(path.join(__dirname, `../static/output/classifieds_importer_${issue.toLowerCase()}_${(new Date()).getFullYear()}_template.csv`)))
  }
}
