module.exports = {
  formatCSV: function (nodeID, issue) {
    const fs = require('fs')
    const path = require('path')
    const convert = require('xml-js')
    const json2csv = require('json2csv')
    const traverse = require('traverse')

    class CSVRow {
      constructor (pageTitle, title, deck, roofline, closingNotes, body, description, pageNumber) {
        this.pageTitle = pageTitle
        this.title = title
        this.deck = deck
        this.roofline = roofline
        this.body = body
        this.closingNotes = closingNotes
        this.description = description
        this.pageNumber = pageNumber
      }
    }

    const fields = [
      'Issue (NID)',
      'Print Section',
      'Roofline',
      'Title',
      'Deck',
      'Page Slug',
      'Page Title',
      'Body',
      'Closing Notes',
      'Description',
      'Page Number',
      'Sequence',
      'Published (0 for not published)'
    ]
    const data = []
    let skipped = []
    fs.readdirSync(path.join(__dirname, '../static/articles/'), function (err, files) {
      if (err) throw err
      return files
    }).filter((e) => {
      return e !== '.keep' && e !== '.DS_Store'
    }).forEach((sourceFile, index) => {
      console.log(`Working on ${sourceFile}`)
      const incopy = fs.readFileSync(path.join(__dirname, '../static/articles/', sourceFile), 'utf8')
      const convertedJSON = convert.xml2js(incopy, { compact: true })
      const tableNodes = traverse(convertedJSON).reduce(function (acc, x) {
        if (this.key === 'Table') {
          acc.push(x.Cell)
        }
        return acc
      }, [])[0]

      if (tableNodes) {
        const output = tableNodes.map((value) => {
          if (value.ParagraphStyleRange.CharacterStyleRange) {
            return value.ParagraphStyleRange.CharacterStyleRange.Content
          } else if (Array.isArray(value.ParagraphStyleRange)) {
            for (let i = 0; i < value.ParagraphStyleRange.length; i++) {
              if (value.ParagraphStyleRange[i].CharacterStyleRange.Content) {
                return value.ParagraphStyleRange[i].CharacterStyleRange.Content
              } else {
                continue
              }
            }
          }
        }).map((value) => {
          if (!value || !value._text) {
            return ' '
          } else {
            return value._text
          }
        }).filter((value, index) => {
          if (index % 2 !== 0) {
            return value
          }
        })
        const row = new CSVRow(...output)
        data.push({
          'Issue (NID)': nodeID,
          'Print Section': ' ',
          'Roofline': row.roofline,
          'Title': row.title,
          'Deck': row.deck,
          'Page Slug': ' ',
          'Page Title': row.pageTitle,
          'Body': row.body,
          'Closing Notes': row.closingNotes,
          'Description': row.description,
          'Page Number': row.pageNumber,
          'Sequence': ' ',
          'Published (0 for not published)': '0'
        })
      } else {
        skipped.push(sourceFile)
      }
    })

    const csv = json2csv({ data: data, fields: fields })
    fs.writeFile(path.join(__dirname, `../static/output/issue_toc_importer_${issue.toLowerCase()}_${(new Date()).getFullYear()}_template.csv`), csv, function (err) {
      if (err) throw err
      if (data.length === 0) {
        console.log('0 rows created. Double check the articles folder.')
      } else {
        console.log(`Success! ${data.length} rows were created, the following ${skipped.length} rows were skipped.`)
        skipped.forEach((i) => {
          console.log(i)
        })
      }
    })
  }
}
