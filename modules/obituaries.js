module.exports = {
  formatObituaries: function (nodeID, issue) {
    const fs = require('fs')
    const path = require('path')
    const json2csv = require('json2csv')
    const cheerio = require('cheerio')
    const mammoth = require('mammoth')

    const fields = [
      'Obit Title',
      'Issue (NID)',
      'Section (Faculty/Staff)',
      'Class Year',
      'Body',
      'Published (0 for unpublished)'
    ]
    const data = []

    mammoth.convertToHtml({path: path.join(__dirname, '../static/obituaries/obits.docx')}).then((result) => {
      const regex = /(’\d{2})/g
      const $ = cheerio.load(result.value)

      $('p').each(function (index) {
        let classYear
        const body = $(this).text()
        const html = $(this).html()

        if (body.match(regex)) {
          classYear = `19${body.match(regex)[0].slice(1)}`

          data.push({
            'Obit Title': `obit #${index + 1}`,
            'Issue (NID)': nodeID,
            'Section (Faculty/Staff)': '',
            'Class Year': classYear,
            'Body': `<p>${html}</p>`,
            'Published (0 for unpublished)': '1'
          })
        } else {
          data.push({
            'Obit Title': `obit #${index + 1}`,
            'Issue NID': nodeID,
            'Section (Faculty/Staff)': '',
            'Class Year': '',
            'Body': `<p>${html}</p>`,
            'Published (0 for unpublished)': '1'
          })
        }
      })
      const csv = json2csv({ data: data, fields: fields })
      fs.writeFile(path.join(__dirname, `../static/output/obituaries_importer_${issue.toLowerCase()}_${(new Date()).getFullYear()}_template.csv`), csv, function (err) {
        if (err) throw err
        if (data.length === 0) {
          console.log('0 rows created. Double check the obituaries folder.')
        } else {
          console.log(`Success! ${data.length} rows created.`)
        }
      })
    }).done()
  }
}
